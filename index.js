let DB = require("./db.js");
const express = require("express");
const app = express();
const port = 3000;
var bodyParser = require("body-parser");
const db = new DB("logs", "logs");
app.use(bodyParser.json());
app.get("/", (req, res) => {
  res.send("Hello World!");
});
app.post("/", async (req, res) => {
    let ret;
  try {
    let ret = await db.insertOne(req.body);
  }
  catch(err) {
      ret = err
  }
res.send(ret)
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
