const MongoClient = require("mongodb").MongoClient;
const uri = "mongodb+srv://elliot:PqzGxz0ldt$7sI3bLIrMiLbGBdudxI@cluster0-hvrcl.mongodb.net/test?retryWrites=true&w=majority";

class mongo {
    constructor(dbName, collectionName) {
        this.dbName = dbName;
        this.collectionName = collectionName;
        this.collection = null;
    }
    start() {
    
        const client = new MongoClient(uri, {
            useNewUrlParser: true
        });
        client.connect(err => {
            if (err) {
                rej(err);
            }
            console.log("connected");
            this.collection = client.db(this.dbName).collection(this.collectionName);
        });
    }

    find(args) {
        return new Promise(function(res, rej) {
            this.collection.find(args).toArray(function(err, docs) {
                if (err) {
                    rej(err);
                }
                res(docs);
            });
        });
    }
    findOne(args) {
        return new Promise(function(res, rej) {
            this.collection.findOne(args).toArray(function(err, docs) {
                if (err) {
                    rej(err);
                }
                res(docs);
            });
        });
    }
    deleteOne(args) {
        return new Promise(function(res, rej) {
            this.collection.deleteOne(args, function(err, result) {
                if (err) {
                    rej(err);
                }
                res(result);
            });
        });
    }
    deleteMany(args) {
        return new Promise(function(res, rej) {
            this.collection.deleteMany(args, function(err, result) {
                if (err) {
                    rej(err);
                }
                res(result);
            });
        });
    }
    insertMany(docs) {
        return new Promise(function(res, rej) {
            this.collection.insertMany(docs, function(err, result) {
                if (err) {
                    rej(err);
                }
                res(result);
            });
        });
    }
    insertOne(doc) {
        return new Promise(function(res, rej) {
            this.collection.insertOne(doc, function(err, result) {
                if (err) {
                    rej(err);
                }
                res(result);
            });
        });
    }
    updateOne(args, newOne) {
        return new Promise(function(res, rej) {
            this.collection.updateOne(args, newOne, function(err, result) {
                if (err) {
                    rej(err);
                }
                res(result);
            });
        });
    }
    size() {
        return new Promise(function(res, rej) {
            this.collection.dataSize(function(err, result) {
                if (err) {
                    rej(err);
                }
                res(result);
            });
        });
    }
    count() {
        return new Promise(function(res, rej) {
            this.collection.count(function(err, result) {
                if (err) {
                    rej(err);
                }
                res(result);
            });
        });
    }
}
module.exports = mongo;